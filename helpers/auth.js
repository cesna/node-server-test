const jwt = require('express-jwt');
const jwtAuthz = require('express-jwt-authz');
const jwksRsa = require('jwks-rsa');

const jwtCheck = jwt({
    secret: jwksRsa.expressJwtSecret({
        cache: true,
        rateLimit: true,
        jwksRequestsPerMinute: 5,
        jwksUri: "https://gytis.eu.auth0.com/.well-known/jwks.json"
    }),
    audience: 'https://bta-books-api.herokuapp.com/',
    issuer: "https://gytis.eu.auth0.com/",
    algorithms: ['RS256']
});

// const removeCars = jwtAuthz([ 'read:messages' ]);
// const checkScopes = jwtAuthz([ 'read:messages' ]);
// const checkScopes = jwtAuthz([ 'read:messages' ]);
const user = [ 'read:cars', 'read:books' ];
const admin = [...user, 'remove:cars', 'create:books' ]
 
const userScoper = jwtAuthz(user);
const adminScopes = jwtAuthz(admin);

module.exports = {jwtCheck, userScoper, adminScopes}